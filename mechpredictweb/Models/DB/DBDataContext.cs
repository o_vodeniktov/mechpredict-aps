﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mechpredictweb.Models.DB
{
    public class DBDataContext : DbContext
    {
        public DbSet<Data_Model> Data_Models { get; set; }
        public DBDataContext(DbContextOptions<DBDataContext> options)
           : base(options)
        {
        }
    }
}
