﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mechpredictweb.Models
{
    public class User
    {
        /// <summary>
        /// Login user
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Password user
        /// </summary>
        public string Password { get; set; }
    }
    public class IndexClass
    {
        public IndexClass()
        {
            Models = new List<SelectListItem>();
        }

        public List<SelectListItem> Models { get; set; }

        public string SelectModel { get; set; }
        public IFormFile File { get; set; }

        public DataResponce Data { get; set; }

    }

    public class DataModelClass
    {
        public DataModelClass()
        {
            Models = new List<SelectListItem>();
            Names = new List<SelectListItem>();
        }

        public List<SelectListItem> Models { get; set; }
        public List<SelectListItem> Names { get; set; }
    }

    public class MeshModels
    {
        public string[] files { get; set; }
    }

    #region dataCalculate
    public class DataResponce
    {
        public Datum[] data { get; set; }
        public Result[] results { get; set; }
    }

    public class Datum
    {
        public float? _ { get; set; }
        public string ИД_СЛЯБА { get; set; }
        public string ПР_ГП { get; set; }
        public string Д_ПРОКАТА { get; set; }
        public long? ЗАКАЗ { get; set; }
        public float? ПОЗИЦИЯ { get; set; }
        public string ПЛАВКА { get; set; }
        public string ТИПТЕХЭСПЦ { get; set; }
        public string ПР_УВС { get; set; }
        public float? ПАРТИЯ { get; set; }
        public string ДИАП_ПАРТ { get; set; }
        public float? N_СЛЯБ { get; set; }
        public string ДИАП_СЛЯБ { get; set; }
        public string МАРКА { get; set; }
        public string ТЕХТРЕБ { get; set; }
        public float? К_МАРКИ { get; set; }
        public float? H_СЛЯБ { get; set; }
        public float? B_СЛЯБ { get; set; }
        public float? L_СЛЯБ { get; set; }
        public float? ВЕС_СЛ { get; set; }
        public float? ВЕСФ_СЛ { get; set; }
        public float? H_ЛИСТ { get; set; }
        public float? B_ЛИСТ { get; set; }
        public float? L_ЛИСТ { get; set; }
        public float? КРАТ { get; set; }
        public float? Д_Н_1_ДЛ { get; set; }
        public float? Д_Н_2_ДЛ { get; set; }
        public float? Д_Н_3_ДЛ { get; set; }
        public float? ПЕЧЬ_РЯД { get; set; }
        public string ЗАГР_ПЕЧЬ { get; set; }
        public string ВЫГР_ПЕЧЬ { get; set; }
        public string ДЛИТ_ПЕЧЬ { get; set; }
        public float? ПЛ_Т_ПЕЧЬ { get; set; }
        public float? ФК_Т_ПЕЧЬ { get; set; }
        public float? СХЕМА_ПРОК { get; set; }
        public float? ДУО_ПРОХ { get; set; }
        public float? ДУО_ХОЛОСТ { get; set; }
        public float? ГИДРОСБИВ { get; set; }
        public float? ДУО_ОБЖ_01 { get; set; }
        public float? ДУО_ОБЖ_02 { get; set; }
        public float? ДУО_ОБЖ_03 { get; set; }
        public float? ДУО_ОБЖ_04 { get; set; }
        public float? ДУО_ОБЖ_05 { get; set; }
        public float? ДУО_ОБЖ_06 { get; set; }
        public float? ДУО_ОБЖ_07 { get; set; }
        public float? ДУО_ОБЖ_08 { get; set; }
        public float? ДУО_ОБЖ_09 { get; set; }
        public float? ДУО_ОБЖ_10 { get; set; }
        public float? ДУО_ОБЖ_11 { get; set; }
        public float? ДУО_ОБЖ_12 { get; set; }
        public float? ДУО_ОБЖ_13 { get; set; }
        public float? ДУО_ОБЖ_14 { get; set; }
        public float? ДУО_ОБЖ_15 { get; set; }
        public float? ДУО_УСС_01 { get; set; }
        public float? ДУО_УСС_02 { get; set; }
        public float? ДУО_УСС_03 { get; set; }
        public float? ДУО_УСС_04 { get; set; }
        public float? ДУО_УСС_05 { get; set; }
        public float? ДУО_УСС_06 { get; set; }
        public float? ДУО_УСС_07 { get; set; }
        public float? ДУО_УСС_08 { get; set; }
        public float? ДУО_УСС_09 { get; set; }
        public float? ДУО_УСС_10 { get; set; }
        public float? ДУО_УСС_11 { get; set; }
        public float? ДУО_УСС_12 { get; set; }
        public float? ДУО_УСС_13 { get; set; }
        public float? ДУО_УСС_14 { get; set; }
        public float? ДУО_УСС_15 { get; set; }
        public float? ДУО_УСП_01 { get; set; }
        public float? ДУО_УСП_02 { get; set; }
        public float? ДУО_УСП_03 { get; set; }
        public float? ДУО_УСП_04 { get; set; }
        public float? ДУО_УСП_05 { get; set; }
        public float? ДУО_УСП_06 { get; set; }
        public float? ДУО_УСП_07 { get; set; }
        public float? ДУО_УСП_08 { get; set; }
        public float? ДУО_УСП_09 { get; set; }
        public float? ДУО_УСП_10 { get; set; }
        public float? ДУО_УСП_11 { get; set; }
        public float? ДУО_УСП_12 { get; set; }
        public float? ДУО_УСП_13 { get; set; }
        public float? ДУО_УСП_14 { get; set; }
        public float? ДУО_УСП_15 { get; set; }
        public float? ДУО_СКР_01 { get; set; }
        public float? ДУО_СКР_02 { get; set; }
        public float? ДУО_СКР_03 { get; set; }
        public float? ДУО_СКР_04 { get; set; }
        public float? ДУО_СКР_05 { get; set; }
        public float? ДУО_СКР_06 { get; set; }
        public float? ДУО_СКР_07 { get; set; }
        public float? ДУО_СКР_08 { get; set; }
        public float? ДУО_СКР_09 { get; set; }
        public float? ДУО_СКР_10 { get; set; }
        public float? ДУО_СКР_11 { get; set; }
        public float? ДУО_СКР_12 { get; set; }
        public float? ДУО_СКР_13 { get; set; }
        public float? ДУО_СКР_14 { get; set; }
        public float? ДУО_СКР_15 { get; set; }
        public float? ДУО_Т_3_ПР { get; set; }
        public float? ДУО_Т_ПОСЛ { get; set; }
        public float? УДО { get; set; }
        public float? УМО { get; set; }
        public float? ПОДКАТ { get; set; }
        public float? КВР_ОБЖ_01 { get; set; }
        public float? КВР_ОБЖ_02 { get; set; }
        public float? КВР_ОБЖ_03 { get; set; }
        public float? КВР_ОБЖ_04 { get; set; }
        public float? КВР_ОБЖ_05 { get; set; }
        public float? КВР_ОБЖ_06 { get; set; }
        public float? КВР_ОБЖ_07 { get; set; }
        public float? КВР_ОБЖ_08 { get; set; }
        public float? КВР_ОБЖ_09 { get; set; }
        public float? КВР_ОБЖ_10 { get; set; }
        public float? КВР_ОБЖ_11 { get; set; }
        public float? КВР_ОБЖ_12 { get; set; }
        public float? КВР_ОБЖ_13 { get; set; }
        public float? КВР_ОБЖ_14 { get; set; }
        public float? КВР_ОБЖ_15 { get; set; }
        public float? КВР_УСС_01 { get; set; }
        public float? КВР_УСС_02 { get; set; }
        public float? КВР_УСС_03 { get; set; }
        public float? КВР_УСС_04 { get; set; }
        public float? КВР_УСС_05 { get; set; }
        public float? КВР_УСС_06 { get; set; }
        public float? КВР_УСС_07 { get; set; }
        public float? КВР_УСС_08 { get; set; }
        public float? КВР_УСС_09 { get; set; }
        public float? КВР_УСС_10 { get; set; }
        public float? КВР_УСС_11 { get; set; }
        public float? КВР_УСС_12 { get; set; }
        public float? КВР_УСС_13 { get; set; }
        public float? КВР_УСС_14 { get; set; }
        public float? КВР_УСС_15 { get; set; }
        public float? КВР_УСП_01 { get; set; }
        public float? КВР_УСП_02 { get; set; }
        public float? КВР_УСП_03 { get; set; }
        public float? КВР_УСП_04 { get; set; }
        public float? КВР_УСП_05 { get; set; }
        public float? КВР_УСП_06 { get; set; }
        public float? КВР_УСП_07 { get; set; }
        public float? КВР_УСП_08 { get; set; }
        public float? КВР_УСП_09 { get; set; }
        public float? КВР_УСП_10 { get; set; }
        public float? КВР_УСП_11 { get; set; }
        public float? КВР_УСП_12 { get; set; }
        public float? КВР_УСП_13 { get; set; }
        public float? КВР_УСП_14 { get; set; }
        public float? КВР_УСП_15 { get; set; }
        public float? КВР_СКР_01 { get; set; }
        public float? КВР_СКР_02 { get; set; }
        public float? КВР_СКР_03 { get; set; }
        public float? КВР_СКР_04 { get; set; }
        public float? КВР_СКР_05 { get; set; }
        public float? КВР_СКР_06 { get; set; }
        public float? КВР_СКР_07 { get; set; }
        public float? КВР_СКР_08 { get; set; }
        public float? КВР_СКР_09 { get; set; }
        public float? КВР_СКР_10 { get; set; }
        public float? КВР_СКР_11 { get; set; }
        public float? КВР_СКР_12 { get; set; }
        public float? КВР_СКР_13 { get; set; }
        public float? КВР_СКР_14 { get; set; }
        public float? КВР_СКР_15 { get; set; }
        public float? DELTA1_2 { get; set; }
        public float? DELTA2_3 { get; set; }
        public float? DELTA3_4 { get; set; }
        public float? DELTA4_5 { get; set; }
        public float? DELTA5_6 { get; set; }
        public float? DELTA6_7 { get; set; }
        public float? DELTA7_8 { get; set; }
        public float? DELTA8_9 { get; set; }
        public float? DELTA9_10 { get; set; }
        public float? DELTA10_11 { get; set; }
        public float? DELTA11_12 { get; set; }
        public float? DELTA12_13 { get; set; }
        public float? DELTA13_14 { get; set; }
        public float? DELTA14_15 { get; set; }
        public float? КВР_Т_ПЛАН { get; set; }
        public float? КВР_Т_1_ПР { get; set; }
        public float? S2_START { get; set; }
        public float? КВР_Т_ПОСЛ { get; set; }
        public float? ВМЕШ_ТЕМП { get; set; }
        public float? КВР_ПРОХ { get; set; }
        public float? КВР_ХОЛОСТ { get; set; }
        public float? УКО_СТРАТ { get; set; }
        public float? УКО_Т_ПЛАН { get; set; }
        public float? УКО_И_ПЛАН { get; set; }
        public string УКО_НАЧАЛО { get; set; }
        public float? УКО_Т_НАЧ { get; set; }
        public float? УКО_Т_КОН { get; set; }
        public float? УКО_ИНТ { get; set; }
        public float? УКО_СКОР { get; set; }
        public string УКО_ВЕРХ_С { get; set; }
        public string УКО_НИЖН_С { get; set; }
        public string НАЧ_ПРОКАТ { get; set; }
        public float? ПЕЧЬ_ДУО { get; set; }
        public float? ДУО_ВРЕМЯ { get; set; }
        public float? ДУО_КВР { get; set; }
        public float? КВР_ВРЕМЯ { get; set; }
        public float? УКО_ВРЕМЯ { get; set; }
        public string КОН_ПРОКАТ { get; set; }
        public float? КОЛ_СЛ_НАЧ { get; set; }
        public float? КОЛ_СЛ_КОН { get; set; }
        public float? КОД_ОШИБКИ { get; set; }
        public float? КОД_ЦЛК { get; set; }
        public float? УГЛ_ЭКВ { get; set; }
        public float? КХС { get; set; }
        public float? C { get; set; }
        public float? SI { get; set; }
        public float? MN { get; set; }
        public float? P { get; set; }
        public float? S { get; set; }
        public float? CR { get; set; }
        public float? NI { get; set; }
        public float? CU { get; set; }
        public float? TI { get; set; }
        public float? AL { get; set; }
        public float? N2 { get; set; }
        public float? NB { get; set; }
        public float? V { get; set; }
        public float? B { get; set; }
        public float? MO { get; set; }
        public float? СТОППИР { get; set; }
        public string NOMP { get; set; }
        public float? VTR { get; set; }
        public string NISP { get; set; }
        public float? ISP_OB { get; set; }
        public float? FPT { get; set; }
        public float? FVS { get; set; }
        public float? FOYD { get; set; }
        public float? FOS { get; set; }
        public float? FPTP { get; set; }
        public float? FVSP { get; set; }
        public float? FOYDP { get; set; }
        public float? FYDR { get; set; }
        public float? TUV1 { get; set; }
        public float? T_KCU { get; set; }
        public float? FUV_1 { get; set; }
        public float? FUV_2 { get; set; }
        public float? FUV_3 { get; set; }
        public float? TUV2 { get; set; }
        public float? T_KCV { get; set; }
        public float? FDV_1 { get; set; }
        public float? FDV_2 { get; set; }
        public float? FDV_3 { get; set; }
        public float? T_KCD { get; set; }
        public float? DWTT1 { get; set; }
        public float? DWTT2 { get; set; }
        public float? T_KCDV { get; set; }
        public float? DVC1 { get; set; }
        public float? DVC2 { get; set; }
        public float? DVC3 { get; set; }
        public float? NB1 { get; set; }
        public float? NB2 { get; set; }
        public float? VPN { get; set; }
        public float? VPV { get; set; }
        public float? FPT05 { get; set; }
        public float? FPTP05 { get; set; }
        public float? PRVLU { get; set; }
        public float? PRVLV { get; set; }
        public float? PRVVC { get; set; }
        public float? PRLDW { get; set; }
    }

    public class Result
    {
        public float? FPT { get; set; }
        public float? FVS { get; set; }
        public float? FUV { get; set; }
        public float? FDV { get; set; }
        public float? DWTT { get; set; }
    }
    #endregion
    #region models


    public class Models_Root
    {
        public Model[] models { get; set; }
    }

    public class Model
    {
        public string name { get; set; }
        public Metrics metrics { get; set; }
        public Plots plots { get; set; }
    }

    public class Metrics
    {
        public FPT FPT { get; set; }
        public FVS FVS { get; set; }
        public FUV FUV { get; set; }
        public FDV FDV { get; set; }
        public DWTT DWTT { get; set; }
    }

    public class FPT
    {
        public int Samples { get; set; }
        public decimal? MAE { get; set; }
        [JsonProperty(PropertyName = "MAE train")]
        public decimal? MAEtrain { get; set; }
        [JsonProperty(PropertyName = "MAE test")]
        public decimal? MAEtest { get; set; }
    }

    public class FVS
    {
        public int Samples { get; set; }
        public decimal? MAE { get; set; }
        [JsonProperty(PropertyName = "MAE train")]
        public decimal? MAEtrain { get; set; }
        [JsonProperty(PropertyName = "MAE test")]
        public decimal? MAEtest { get; set; }
    }

    public class FUV
    {
        public int Samples { get; set; }
        public decimal? MAE { get; set; }
        [JsonProperty(PropertyName = "MAE train")]
        public decimal? MAEtrain { get; set; }
        [JsonProperty(PropertyName = "MAE test")]
        public decimal? MAEtest { get; set; }
    }

    public class FDV
    {
        public int Samples { get; set; }
        public decimal? MAE { get; set; }
        [JsonProperty(PropertyName = "MAE train")]
        public decimal? MAEtrain { get; set; }
        [JsonProperty(PropertyName = "MAE test")]
        public decimal? MAEtest { get; set; }
    }

    public class DWTT
    {
        public int Samples { get; set; }
        public decimal? MAE { get; set; }
        [JsonProperty(PropertyName = "MAE train")]
        public decimal? MAEtrain { get; set; }
        [JsonProperty(PropertyName = "MAE test")]
        public decimal? MAEtest { get; set; }
    }

    public class Plots
    {
        public string DWTT { get; set; }
        public string FDV { get; set; }
        public string FPT { get; set; }
        public string FUV { get; set; }
        public string FVS { get; set; }
    }


    #endregion

   /* public class Rootobject
    {
        public Datum[] data { get; set; }
        public Result[] results { get; set; }
    }

    public class Datum
    {
        public int _ { get; set; }
        public string ИД_СЛЯБА { get; set; }
        public string ПР_ГП { get; set; }
        public string Д_ПРОКАТА { get; set; }
        public long ЗАКАЗ { get; set; }
        public int ПОЗИЦИЯ { get; set; }
        public string ПЛАВКА { get; set; }
        public string ТИПТЕХЭСПЦ { get; set; }
        public string ПР_УВС { get; set; }
        public int ПАРТИЯ { get; set; }
        public string ДИАП_ПАРТ { get; set; }
        public int N_СЛЯБ { get; set; }
        public string ДИАП_СЛЯБ { get; set; }
        public string МАРКА { get; set; }
        public string ТЕХТРЕБ { get; set; }
        public int К_МАРКИ { get; set; }
        public float H_СЛЯБ { get; set; }
        public float B_СЛЯБ { get; set; }
        public int L_СЛЯБ { get; set; }
        public float ВЕС_СЛ { get; set; }
        public float ВЕСФ_СЛ { get; set; }
        public float H_ЛИСТ { get; set; }
        public float B_ЛИСТ { get; set; }
        public int L_ЛИСТ { get; set; }
        public int КРАТ { get; set; }
        public int Д_Н_1_ДЛ { get; set; }
        public float? Д_Н_2_ДЛ { get; set; }
        public object Д_Н_3_ДЛ { get; set; }
        public int ПЕЧЬ_РЯД { get; set; }
        public string ЗАГР_ПЕЧЬ { get; set; }
        public string ВЫГР_ПЕЧЬ { get; set; }
        public string ДЛИТ_ПЕЧЬ { get; set; }
        public int ПЛ_Т_ПЕЧЬ { get; set; }
        public int ФК_Т_ПЕЧЬ { get; set; }
        public int СХЕМА_ПРОК { get; set; }
        public int ДУО_ПРОХ { get; set; }
        public int ДУО_ХОЛОСТ { get; set; }
        public int ГИДРОСБИВ { get; set; }
        public float ДУО_ОБЖ_01 { get; set; }
        public float ДУО_ОБЖ_02 { get; set; }
        public float ДУО_ОБЖ_03 { get; set; }
        public float ДУО_ОБЖ_04 { get; set; }
        public float ДУО_ОБЖ_05 { get; set; }
        public float ДУО_ОБЖ_06 { get; set; }
        public float ДУО_ОБЖ_07 { get; set; }
        public float ДУО_ОБЖ_08 { get; set; }
        public float ДУО_ОБЖ_09 { get; set; }
        public float ДУО_ОБЖ_10 { get; set; }
        public int ДУО_ОБЖ_11 { get; set; }
        public object ДУО_ОБЖ_12 { get; set; }
        public object ДУО_ОБЖ_13 { get; set; }
        public object ДУО_ОБЖ_14 { get; set; }
        public object ДУО_ОБЖ_15 { get; set; }
        public float ДУО_УСС_01 { get; set; }
        public float ДУО_УСС_02 { get; set; }
        public float ДУО_УСС_03 { get; set; }
        public int ДУО_УСС_04 { get; set; }
        public int ДУО_УСС_05 { get; set; }
        public int ДУО_УСС_06 { get; set; }
        public int ДУО_УСС_07 { get; set; }
        public int ДУО_УСС_08 { get; set; }
        public int ДУО_УСС_09 { get; set; }
        public int ДУО_УСС_10 { get; set; }
        public float ДУО_УСС_11 { get; set; }
        public object ДУО_УСС_12 { get; set; }
        public object ДУО_УСС_13 { get; set; }
        public object ДУО_УСС_14 { get; set; }
        public object ДУО_УСС_15 { get; set; }
        public float ДУО_УСП_01 { get; set; }
        public float ДУО_УСП_02 { get; set; }
        public float ДУО_УСП_03 { get; set; }
        public int ДУО_УСП_04 { get; set; }
        public int ДУО_УСП_05 { get; set; }
        public int ДУО_УСП_06 { get; set; }
        public int ДУО_УСП_07 { get; set; }
        public int ДУО_УСП_08 { get; set; }
        public int ДУО_УСП_09 { get; set; }
        public int ДУО_УСП_10 { get; set; }
        public float ДУО_УСП_11 { get; set; }
        public object ДУО_УСП_12 { get; set; }
        public object ДУО_УСП_13 { get; set; }
        public object ДУО_УСП_14 { get; set; }
        public object ДУО_УСП_15 { get; set; }
        public float ДУО_СКР_01 { get; set; }
        public float ДУО_СКР_02 { get; set; }
        public float ДУО_СКР_03 { get; set; }
        public float ДУО_СКР_04 { get; set; }
        public float ДУО_СКР_05 { get; set; }
        public float ДУО_СКР_06 { get; set; }
        public float ДУО_СКР_07 { get; set; }
        public float ДУО_СКР_08 { get; set; }
        public float ДУО_СКР_09 { get; set; }
        public float ДУО_СКР_10 { get; set; }
        public float ДУО_СКР_11 { get; set; }
        public object ДУО_СКР_12 { get; set; }
        public object ДУО_СКР_13 { get; set; }
        public object ДУО_СКР_14 { get; set; }
        public object ДУО_СКР_15 { get; set; }
        public int ДУО_Т_3_ПР { get; set; }
        public int ДУО_Т_ПОСЛ { get; set; }
        public int УДО { get; set; }
        public float? УМО { get; set; }
        public int ПОДКАТ { get; set; }
        public float КВР_ОБЖ_01 { get; set; }
        public float КВР_ОБЖ_02 { get; set; }
        public float КВР_ОБЖ_03 { get; set; }
        public float КВР_ОБЖ_04 { get; set; }
        public float КВР_ОБЖ_05 { get; set; }
        public float КВР_ОБЖ_06 { get; set; }
        public float КВР_ОБЖ_07 { get; set; }
        public float? КВР_ОБЖ_08 { get; set; }
        public float? КВР_ОБЖ_09 { get; set; }
        public object КВР_ОБЖ_10 { get; set; }
        public object КВР_ОБЖ_11 { get; set; }
        public object КВР_ОБЖ_12 { get; set; }
        public object КВР_ОБЖ_13 { get; set; }
        public object КВР_ОБЖ_14 { get; set; }
        public object КВР_ОБЖ_15 { get; set; }
        public int КВР_УСС_01 { get; set; }
        public int КВР_УСС_02 { get; set; }
        public int КВР_УСС_03 { get; set; }
        public int КВР_УСС_04 { get; set; }
        public int КВР_УСС_05 { get; set; }
        public int КВР_УСС_06 { get; set; }
        public int КВР_УСС_07 { get; set; }
        public float? КВР_УСС_08 { get; set; }
        public float? КВР_УСС_09 { get; set; }
        public object КВР_УСС_10 { get; set; }
        public object КВР_УСС_11 { get; set; }
        public object КВР_УСС_12 { get; set; }
        public object КВР_УСС_13 { get; set; }
        public object КВР_УСС_14 { get; set; }
        public object КВР_УСС_15 { get; set; }
        public int КВР_УСП_01 { get; set; }
        public int КВР_УСП_02 { get; set; }
        public int КВР_УСП_03 { get; set; }
        public int КВР_УСП_04 { get; set; }
        public int КВР_УСП_05 { get; set; }
        public int КВР_УСП_06 { get; set; }
        public int КВР_УСП_07 { get; set; }
        public float? КВР_УСП_08 { get; set; }
        public float? КВР_УСП_09 { get; set; }
        public object КВР_УСП_10 { get; set; }
        public object КВР_УСП_11 { get; set; }
        public object КВР_УСП_12 { get; set; }
        public object КВР_УСП_13 { get; set; }
        public object КВР_УСП_14 { get; set; }
        public object КВР_УСП_15 { get; set; }
        public float КВР_СКР_01 { get; set; }
        public float КВР_СКР_02 { get; set; }
        public float КВР_СКР_03 { get; set; }
        public float КВР_СКР_04 { get; set; }
        public float КВР_СКР_05 { get; set; }
        public float КВР_СКР_06 { get; set; }
        public float КВР_СКР_07 { get; set; }
        public float? КВР_СКР_08 { get; set; }
        public float? КВР_СКР_09 { get; set; }
        public object КВР_СКР_10 { get; set; }
        public object КВР_СКР_11 { get; set; }
        public object КВР_СКР_12 { get; set; }
        public object КВР_СКР_13 { get; set; }
        public object КВР_СКР_14 { get; set; }
        public object КВР_СКР_15 { get; set; }
        public float DELTA1_2 { get; set; }
        public float DELTA2_3 { get; set; }
        public float DELTA3_4 { get; set; }
        public float DELTA4_5 { get; set; }
        public float DELTA5_6 { get; set; }
        public float DELTA6_7 { get; set; }
        public float? DELTA7_8 { get; set; }
        public float? DELTA8_9 { get; set; }
        public object DELTA9_10 { get; set; }
        public object DELTA10_11 { get; set; }
        public object DELTA11_12 { get; set; }
        public object DELTA12_13 { get; set; }
        public object DELTA13_14 { get; set; }
        public object DELTA14_15 { get; set; }
        public int КВР_Т_ПЛАН { get; set; }
        public float КВР_Т_1_ПР { get; set; }
        public int S2_START { get; set; }
        public float КВР_Т_ПОСЛ { get; set; }
        public float? ВМЕШ_ТЕМП { get; set; }
        public int КВР_ПРОХ { get; set; }
        public float? КВР_ХОЛОСТ { get; set; }
        public int УКО_СТРАТ { get; set; }
        public int УКО_Т_ПЛАН { get; set; }
        public int УКО_И_ПЛАН { get; set; }
        public object УКО_НАЧАЛО { get; set; }
        public object УКО_Т_НАЧ { get; set; }
        public object УКО_Т_КОН { get; set; }
        public object УКО_ИНТ { get; set; }
        public object УКО_СКОР { get; set; }
        public object УКО_ВЕРХ_С { get; set; }
        public object УКО_НИЖН_С { get; set; }
        public string НАЧ_ПРОКАТ { get; set; }
        public float ПЕЧЬ_ДУО { get; set; }
        public float ДУО_ВРЕМЯ { get; set; }
        public float ДУО_КВР { get; set; }
        public float КВР_ВРЕМЯ { get; set; }
        public object УКО_ВРЕМЯ { get; set; }
        public string КОН_ПРОКАТ { get; set; }
        public int КОЛ_СЛ_НАЧ { get; set; }
        public int КОЛ_СЛ_КОН { get; set; }
        public int КОД_ОШИБКИ { get; set; }
        public float? КОД_ЦЛК { get; set; }
        public float УГЛ_ЭКВ { get; set; }
        public float КХС { get; set; }
        public float C { get; set; }
        public float SI { get; set; }
        public float MN { get; set; }
        public float P { get; set; }
        public float S { get; set; }
        public float CR { get; set; }
        public float NI { get; set; }
        public float CU { get; set; }
        public float TI { get; set; }
        public float AL { get; set; }
        public float N2 { get; set; }
        public float NB { get; set; }
        public int V { get; set; }
        public float B { get; set; }
        public float MO { get; set; }
        public int СТОППИР { get; set; }
        public int NOMP { get; set; }
        public int VTR { get; set; }
        public int NISP { get; set; }
        public int ISP_OB { get; set; }
        public float FPT { get; set; }
        public float FVS { get; set; }
        public float FOYD { get; set; }
        public object FOS { get; set; }
        public object FPTP { get; set; }
        public object FVSP { get; set; }
        public object FOYDP { get; set; }
        public object FYDR { get; set; }
        public int TUV1 { get; set; }
        public int T_KCU { get; set; }
        public float FUV_1 { get; set; }
        public float FUV_2 { get; set; }
        public float FUV_3 { get; set; }
        public int TUV2 { get; set; }
        public int T_KCV { get; set; }
        public float FDV_1 { get; set; }
        public float FDV_2 { get; set; }
        public float FDV_3 { get; set; }
        public int T_KCD { get; set; }
        public int DWTT1 { get; set; }
        public int DWTT2 { get; set; }
        public object T_KCDV { get; set; }
        public object DVC1 { get; set; }
        public object DVC2 { get; set; }
        public object DVC3 { get; set; }
        public object NB1 { get; set; }
        public object NB2 { get; set; }
        public object VPN { get; set; }
        public object VPV { get; set; }
        public object FPT05 { get; set; }
        public object FPTP05 { get; set; }
        public object PRVLU { get; set; }
        public object PRVLV { get; set; }
        public object PRVVC { get; set; }
        public object PRLDW { get; set; }
    }

    public class Result
    {
        public float FPT { get; set; }
        public float FVS { get; set; }
        public float FUV { get; set; }
        public float FDV { get; set; }
        public float DWTT { get; set; }
    }*/


    // public class 
}
