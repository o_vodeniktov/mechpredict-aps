﻿function load(model) {
    alert('123');
    if (model !== null) alert(model);
}

function LoadModel() {
    $.ajax({
        type: "POST",
        url: "/Home/GetSelectedModel",
        // data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            if (r == null)
                alert("Ошибка загрузки данных");
            else {
                var table_data = "#table_data";
                var table_result = "#table_result";
                var tmp = JSON.parse(r);
                buildHtmlTable(tmp, table_data, table_result);
            }
        },
        failure: function (r) {
            alert(r.d);
        },
        error: function (r) {
            alert(r.d);
        }
    });
}
function buildHtmlTable(myList, data, results) {
   
    $(data).empty();
    $(results).empty();
    var columns = addAllColumnHeaders(myList.items, data);
    var columns_result = addAllColumnHeadersResults(myList.items, results);

    for (var i = 0; i < myList.items.length; i++) {
        var row$ = $('<tr/>');
        var row_r$ = $('<tr/>');
        for (var colIndex = 0; colIndex < columns.length; colIndex++) {
            var cellValue = null;
            if (Array.isArray(myList.items[i].data[columns[colIndex]])) {
                cellValue = "";
                var arrayLenght = myList.items[i].data[columns[colIndex]].length;
                for (var j = 0; j < arrayLenght; j++) {
                    cellValue += myList.items[i].data[columns[colIndex]][j] + ';';
                }
            }
            else {
                cellValue = myList.items[i].data[columns[colIndex]];
            }
            if (cellValue == null) cellValue = "";
            row$.append($('<td contenteditable="true"/>').html(cellValue));
        }
        for (var colIndex = 0; colIndex < columns_result.length; colIndex++) {
            var cellValue_1 = null;
            cellValue_1 = myList.items[i].results[columns_result[colIndex]];
            if (cellValue_1 == null) cellValue_1 = "";
            row_r$.append($('<td style="color:green;font-weight:bolder"/>').html(cellValue_1));
        }
        $(data).append(row$);
        $(results).append(row_r$);
    }
}
function addAllColumnHeaders(myList, selector) {
    var columnSet = [];
    var headerTr$ = $('<tr  />');

    for (var i = 0; i < myList.length; i++) {
        var rowHash = myList[i].data;
        for (var key in rowHash) {
            if ($.inArray(key, columnSet) == -1) {
                columnSet.push(key);
                headerTr$.append($('<th scope="col"/>').html(key));
            }
        }
    }
    $(selector).append(headerTr$);
    return columnSet;
}
function addAllColumnHeadersResults(myList, selector) {
    var columnSet = [];
    var headerTr$ = $('<tr  />');

    for (var i = 0; i < myList.length; i++) {
        var rowHash = myList[i].results;
        for (var key in rowHash) {
            if ($.inArray(key, columnSet) == -1) {
                columnSet.push(key);
                headerTr$.append($('<th scope="col"/>').html(key));
            }
        }
    }
    $(selector).append(headerTr$);
    return columnSet;
}

function GetValues() {
    var numberOfrows = document.getElementById("table_data").rows.length;
    var numberoftds = document.getElementById("table_data").rows[0].cells.length;
    var data = '{ "items" : [';
    for (var i = 1; i < numberOfrows; i++) {
        var datarow = '{ "data" : {';
        for (var j = 0; j < numberoftds; j++) {
            datarow += '"' + document.getElementById("table_data").rows[0].cells.item(j).innerHTML + '" : ' + document.getElementById("table_data").rows[i].cells.item(j).innerHTML;
            if (j !== (numberoftds - 1))
                datarow += ',';
        }
        data += datarow + '}';
        if (i !== (numberOfrows - 1))
            data += ',';
    }
    alert(data);
}
