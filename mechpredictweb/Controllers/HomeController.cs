﻿using mechpredictweb.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace mechpredictweb.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        IWebHostEnvironment _appEnvironment;
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index(string sel_model = null, string json_data = null)
        {
            byte[] login;
            if (HttpContext.Session.TryGetValue("login", out login))
            {
                var data = Get("https://d5de61g5k2rrshd2eq8j.apigw.yandexcloud.net/models");
                var models = Newtonsoft.Json.JsonConvert.DeserializeObject<MeshModels>(data);
                IndexClass model = new IndexClass();
                if (model.Models != null)
                    foreach (var item in models.files)
                    {
                        model.Models.Add(new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem() { Text = item, Value = item });
                    }
                if (!string.IsNullOrWhiteSpace(sel_model))
                    model.SelectModel = sel_model;
                if (!string.IsNullOrWhiteSpace(json_data))
                    model.Data = Newtonsoft.Json.JsonConvert.DeserializeObject<DataResponce>(json_data);
                return View(model);
            }
            else
            {
                return RedirectToAction("Login");
            }

        }

        public IActionResult Models()
        {
            byte[] login;
            if (HttpContext.Session.TryGetValue("login", out login))
            {
                var data = Get("https://d5de61g5k2rrshd2eq8j.apigw.yandexcloud.net/models2");
                var models = Newtonsoft.Json.JsonConvert.DeserializeObject<Models_Root>(data);
                return View(models);
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        public IActionResult CreateModels()
        {
            byte[] login;
            if (HttpContext.Session.TryGetValue("login", out login))
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateModel(IFormFile uploadedFile)
        {

            byte[] login;
            if (HttpContext.Session.TryGetValue("login", out login))
            {
                if (uploadedFile != null)
                {
                    byte[] data;
                    using (var Stream = new MemoryStream())
                    {
                        await uploadedFile.CopyToAsync(Stream);
                        data = Stream.ToArray();
                    }
                    string response = await PostAttachment(data, new Uri("https://d5de61g5k2rrshd2eq8j.apigw.yandexcloud.net/models"));
                }
                return RedirectToAction("Models");
            }
            else
            {
                return RedirectToAction("Login");
            }

        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<IActionResult> Calculate(string model, IFormFile File)
        {
            try
            {
                byte[] data;
                string response;
                using (var stream = new MemoryStream())
                {
                    await File.CopyToAsync(stream);
                    data = stream.ToArray();
                    response = PostFileAndParmetr("https://d5de61g5k2rrshd2eq8j.apigw.yandexcloud.net/prediction", model, data);

                }
                var data_response = Get("https://d5de61g5k2rrshd2eq8j.apigw.yandexcloud.net/models");
                var models = Newtonsoft.Json.JsonConvert.DeserializeObject<MeshModels>(data_response);
                IndexClass index = new IndexClass();
                if (index.Models != null)
                    foreach (var item in models.files)
                    {
                        index.Models.Add(new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem() { Text = item, Value = item });
                    }
                if (!string.IsNullOrWhiteSpace(model))
                    index.SelectModel = model;
                if (!string.IsNullOrWhiteSpace(response))
                    index.Data = Newtonsoft.Json.JsonConvert.DeserializeObject<DataResponce>(response);
                return View(index);
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        private string[] names = new string[] { "ИД_СЛЯБА", "ПР_ГП", "Д_ПРОКАТА", "ЗАКАЗ", "ПОЗИЦИЯ", "ПЛАВКА", "ТИПТЕХЭСПЦ", "ПР_УВС", "ПАРТИЯ int", "ДИАП_ПАРТ", "N_СЛЯБ", "ДИАП_СЛЯБ", "МАРКА", "ТЕХТРЕБ", "К_МАРКИ", "H_СЛЯБ", "B_СЛЯБ", "L_СЛЯБ", "ВЕС_СЛ", "ВЕСФ_СЛ", "H_ЛИСТ", "B_ЛИСТ", "L_ЛИСТ", "КРАТ", "Д_Н_1_ДЛ", "Д_Н_2_ДЛ", "Д_Н_3_ДЛ", "ПЕЧЬ_РЯД", "ЗАГР_ПЕЧЬ", "ВЫГР_ПЕЧЬ", "ДЛИТ_ПЕЧЬ", "ПЛ_Т_ПЕЧЬ", "ФК_Т_ПЕЧЬ", "СХЕМА_ПРОК", "ДУО_ПРОХ", "ДУО_ХОЛОСТ", "ГИДРОСБИВ", "ДУО_ОБЖ_01", "ДУО_ОБЖ_02", "ДУО_ОБЖ_03", "ДУО_ОБЖ_04", "ДУО_ОБЖ_05", "ДУО_ОБЖ_06", "ДУО_ОБЖ_07", "ДУО_ОБЖ_08", "ДУО_ОБЖ_09", "ДУО_ОБЖ_10", "ДУО_ОБЖ_11", "ДУО_ОБЖ_12", "ДУО_ОБЖ_13", "ДУО_ОБЖ_14", "ДУО_ОБЖ_15", "ДУО_УСС_01", "ДУО_УСС_02", "ДУО_УСС_03", "ДУО_УСС_04", "ДУО_УСС_05", "ДУО_УСС_06", "ДУО_УСС_07", "ДУО_УСС_08", "ДУО_УСС_09", "ДУО_УСС_10", "ДУО_УСС_11", "ДУО_УСС_12", "ДУО_УСС_13", "ДУО_УСС_14", "ДУО_УСС_15", "ДУО_УСП_01", "ДУО_УСП_02", "ДУО_УСП_03", "ДУО_УСП_04", "ДУО_УСП_05", "ДУО_УСП_06", "ДУО_УСП_07", "ДУО_УСП_08", "ДУО_УСП_09", "ДУО_УСП_10", "ДУО_УСП_11", "ДУО_УСП_12", "ДУО_УСП_13", "ДУО_УСП_14", "ДУО_УСП_15", "ДУО_СКР_01", "ДУО_СКР_02", "ДУО_СКР_03", "ДУО_СКР_04", "ДУО_СКР_05", "ДУО_СКР_06", "ДУО_СКР_07", "ДУО_СКР_08", "ДУО_СКР_09", "ДУО_СКР_10", "ДУО_СКР_11", "ДУО_СКР_12", "ДУО_СКР_13", "ДУО_СКР_14", "ДУО_СКР_15", "ДУО_Т_3_ПР", "ДУО_Т_ПОСЛ", "УДО", "УМО", "ПОДКАТ", "КВР_ОБЖ_01", "КВР_ОБЖ_02", "КВР_ОБЖ_03", "КВР_ОБЖ_04", "КВР_ОБЖ_05", "КВР_ОБЖ_06", "КВР_ОБЖ_07", "КВР_ОБЖ_08", "КВР_ОБЖ_09", "КВР_ОБЖ_10", "КВР_ОБЖ_11", "КВР_ОБЖ_12", "КВР_ОБЖ_13", "КВР_ОБЖ_14", "КВР_ОБЖ_15", "КВР_УСС_01", "КВР_УСС_02", "КВР_УСС_03", "КВР_УСС_04", "КВР_УСС_05", "КВР_УСС_06", "КВР_УСС_07", "КВР_УСС_08", "КВР_УСС_09", "КВР_УСС_10", "КВР_УСС_11", "КВР_УСС_12", "КВР_УСС_13", "КВР_УСС_14", "КВР_УСС_15", "КВР_УСП_01", "КВР_УСП_02", "КВР_УСП_03", "КВР_УСП_04", "КВР_УСП_05", "КВР_УСП_06", "КВР_УСП_07", "КВР_УСП_08", "КВР_УСП_09", "КВР_УСП_10", "КВР_УСП_11", "КВР_УСП_12", "КВР_УСП_13", "КВР_УСП_14", "КВР_УСП_15", "КВР_СКР_01", "КВР_СКР_02", "КВР_СКР_03", "КВР_СКР_04", "КВР_СКР_05", "КВР_СКР_06", "КВР_СКР_07", "КВР_СКР_08", "КВР_СКР_09", "КВР_СКР_10", "КВР_СКР_11", "КВР_СКР_12", "КВР_СКР_13", "КВР_СКР_14", "КВР_СКР_15", "DELTA1_2", "DELTA2_3", "DELTA3_4", "DELTA4_5", "DELTA5_6", "DELTA6_7", "DELTA7_8", "DELTA8_9", "DELTA9_10", "DELTA10_11", "DELTA11_12", "DELTA12_13", "DELTA13_14", "DELTA14_15", "КВР_Т_ПЛАН", "КВР_Т_1_ПР", "S2_START", "КВР_Т_ПОСЛ", "ВМЕШ_ТЕМП", "КВР_ПРОХ", "КВР_ХОЛОСТ", "УКО_СТРАТ", "УКО_Т_ПЛАН", "УКО_И_ПЛАН", "УКО_НАЧАЛО", "УКО_Т_НАЧ", "УКО_Т_КОН", "УКО_ИНТ", "УКО_СКОР", "УКО_ВЕРХ_С", "УКО_НИЖН_С", "НАЧ_ПРОКАТ", "ПЕЧЬ_ДУО", "ДУО_ВРЕМЯ", "ДУО_КВР", "КВР_ВРЕМЯ", "УКО_ВРЕМЯ", "КОН_ПРОКАТ", "КОЛ_СЛ_НАЧ", "КОЛ_СЛ_КОН", "КОД_ОШИБКИ", "КОД_ЦЛК", "УГЛ_ЭКВ", "КХС", "C", "SI", "MN", "P", "S", "CR", "NI", "CU", "TI", "AL", "N2", "NB", "V", "B", "MO", "СТОППИР", "NOMP", "VTR", "NISP", "ISP_OB", "FPT", "FVS", "FOYD", "FOS", "FPTP", "FVSP", "FOYDP", "FYDR", "TUV1", "T_KCU", "FUV_1", "FUV_2", "FUV_3", "TUV2", "T_KCV", "FDV_1", "FDV_2", "FDV_3", "T_KCD", "DWTT1", "DWTT2", "T_KCDV", "DVC1", "DVC2", "DVC3", "NB1", "NB2", "VPN", "VPV", "FPT05", "FPTP05", "PRVLU", "PRVLV", "PRVVC", "PRLDW" };

        public IActionResult DataModel()
        {

            return View();
        }





        [HttpPost]
        public JsonResult GetSelectedModel()
        {
            var data = Post("https://d5de61g5k2rrshd2eq8j.apigw.yandexcloud.net/prediction", "");
            return Json(data);
        }
        private static string Get(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            return reader.ReadToEnd();
        }

        /// <summary>
        /// Метод отправки запросов
        /// </summary>
        /// <param name="url">POST URL</param>
        /// <returns></returns>
        private static string Post(string url, string data)
        {
            string strReturn = null;
            try
            {
                byte[] buffer = Encoding.UTF8.GetBytes(data);
                HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(url);
                WebReq.KeepAlive = false;
                WebReq.ProtocolVersion = HttpVersion.Version11;
                WebReq.Method = "POST";
                WebReq.ContentType = "application/json";
                WebReq.ContentLength = buffer.Length;
                using (Stream PostData = WebReq.GetRequestStream())
                {
                    PostData.Write(buffer, 0, buffer.Length);
                    PostData.Close();
                }
                HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();
                Stream WebResponse = WebResp.GetResponseStream();
                StreamReader _response = new StreamReader(WebResponse);
                strReturn = _response.ReadToEnd();
                return strReturn;

            }
            catch (WebException ex1)
            {
                return strReturn;
            }
        }

        private async Task<string> PostAttachment(byte[] data, Uri url, string contentType = "multipart/form-data")
        {
            HttpContent content = new ByteArrayContent(data);

            content.Headers.ContentType = new MediaTypeHeaderValue(contentType);

            using (var form = new MultipartFormDataContent())
            {
                form.Add(content);

                using (var client = new HttpClient())
                {
                    var response = await client.PostAsync(url, form);
                    return await response.Content.ReadAsStringAsync();
                }
            }
        }

        private string PostFileAndParmetr(string actionUrl, string paramString, byte[] dataBytes)
        {

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(actionUrl + "?model=" + paramString);
                request.Method = "POST";
                request.ContentType = "application/octet-stream";
                request.ContentLength = dataBytes.Length;
                Stream dataStream = request.GetRequestStream();
                using (Stream PostData = request.GetRequestStream())
                {
                    PostData.Write(dataBytes, 0, dataBytes.Length);
                    PostData.Close();
                }
                HttpWebResponse WebResp = (HttpWebResponse)request.GetResponse();
                Stream WebResponse = WebResp.GetResponseStream();
                StreamReader _response = new StreamReader(WebResponse);
                return _response.ReadToEnd();
            }
            catch
            {
                return "";
            }

        }
        public IActionResult Login()
        {
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> Logined(Models.User user)
        {
            if (user.Name == "admin" && user.Password == "pe7u27qs")
            {
                HttpContext.Session.Set("login", Encoding.ASCII.GetBytes(user.Name));
                ViewBag.Name = user.Name;
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.error = "Не верный логин/пароль";
                return View("Login");
            }
        }
    }
}
